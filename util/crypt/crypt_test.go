package crypt

import "testing"

func TestSha1(t *testing.T) {
	d := "test"
	s := Sha1(d)
	t.Logf("%s", s)
	//a94a8fe5ccb19ba61c4c0873d391e987982fbbd3
}

func TestMD5(t *testing.T) {
	d := "test"
	s := MD5([]byte(d))
	t.Logf("%s", s)
	//098f6bcd4621d373cade4e832627b4f6
}

func TestHamSha1(t *testing.T) {
	d := "098f6bcd4621d373cade4e832627b4f6"
	s := HamSha1(d)
	t.Logf("%s", s)
	//QCig41asyUf80r+/AM7xHhKNSEo=
}

func TestHamSha1Key(t *testing.T) {
	d := "test"
	k := "security"
	s := HamSha1Key(d, []byte(k))
	t.Logf("%s", s)
	//ZiBIOPgeYwqFJLpeN9n/SMhP19E=
}
