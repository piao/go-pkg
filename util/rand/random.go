package rand

import (
	"math/rand"
	"time"
)

var (
	r = rand.New(rand.NewSource(time.Now().UnixNano()))
)

func Int() int {
	return r.Int()
}

func IntN(n int) int {
	return r.Intn(n)
}

func Int32() int32 {
	return r.Int31()
}

func Uint32() uint32 {
	return r.Uint32()
}

func Int32N(n int32) int32 {
	return r.Int31n(n)
}

func Int64() int64 {
	return r.Int63()
}

func Int64N(n int64) int64 {
	return r.Int63n(n)
}

func Float32() float32 {
	return r.Float32()
}

func Float64() float64 {
	return r.Float64()
}

func Seed() {
	r.Seed(time.Now().UnixNano())
}
