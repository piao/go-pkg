package db

import (
	"bytes"
	"encoding/gob"
	"encoding/json"
	"fmt"
	"log"
	"testing"

	"github.com/cockroachdb/pebble"
	"github.com/cockroachdb/pebble/vfs"
)

func TestNewPebble(t *testing.T) {
	db, err := NewPebble("./pebble")
	if err != nil {
		t.Fatal(err)
	}
	var (
		key   = []byte("test")
		value = []byte("你好，world")
	)
	if err = db.Set(key, value, nil); err != nil {
		t.Error(err)
	}

	result, err := db.Get(key)
	if err != nil {
		t.Error(err)
	}
	if string(value) != string(result) {
		t.Error("mismatch")
	}
}

type TickSum struct {
	Symbol    string  `json:"s"`
	Volume    float64 `json:"v"`
	Quote     float64 `json:"q"`
	Close     float64 `json:"c"`
	CloseTime uint    `json:"t"`
}

func TestJsonAndGob(t *testing.T) {
	tk := TickSum{
		Symbol:    "123456789",
		Volume:    12,
		Quote:     34,
		Close:     56,
		CloseTime: 78,
	}

	bs, _ := json.Marshal(tk)
	buf := bytes.Buffer{}
	_ = gob.NewEncoder(&buf).Encode(tk)

	t.Log(len(bs), buf.Len(), string(bs), buf.String())
}

func TestIter(t *testing.T) {
	// db, err := NewPebble("./pebble")
	pdb, err := pebble.Open("", nil)
	if err != nil {
		t.Fatal(err)
	}
	db := &Pebble{DB: pdb}

	keys := []string{"hello.003", "hello.002", "hello.004", "hello.9105", "hello.005"}
	for _, key := range keys {
		if err := db.Set([]byte(key), []byte(key), pebble.Sync); err != nil {
			log.Fatal(err)
		}
	}
	fmt.Println("last", Bytes2String(db.LastKey([]byte("hello.001"), []byte("hello.a"))))
	iter := db.NewIter(&pebble.IterOptions{
		LowerBound: []byte("hello."),
		//UpperBound: []byte("hello.009"),
	})
	iter.SeekLT([]byte("hello.005"))
	//iter.Prev()
	fmt.Println(string(iter.Key()))
	if iter.SeekGE([]byte("hello.001")); iter.Valid() {
		if "hello.001" != Bytes2String(iter.Key()) {
			print(false)
		}
		fmt.Printf("%s\n", iter.Key())
	}
	if iter.SeekGE([]byte("hello.003")); iter.Valid() {
		fmt.Printf("%s\n", iter.Key())
	}
	if iter.SeekGE([]byte("hello.005")); iter.Valid() {
		fmt.Printf("%s\n", iter.Key())
	}

	//for iter.First(); iter.Valid(); iter.Next() {
	//	fmt.Println(iter.HasPointAndRange())
	//	fmt.Printf("%s\n", iter.Key())
	//}
	if err := iter.Close(); err != nil {
		log.Fatal(err)
	}
	if err := db.Close(); err != nil {
		log.Fatal(err)
	}
}

func Test_IterDelete(t *testing.T) {
	db, err := pebble.Open("", &pebble.Options{FS: vfs.NewMem()})
	if err != nil {
		t.Fatal(err)
	}

	keys := []string{"hello.003", "hello.002", "hello.004", "hello.9105", "hello.005"}
	for _, key := range keys {
		if err := db.Set([]byte(key), []byte(key), pebble.Sync); err != nil {
			t.Fatal(err)
		}
	}

	value, closer, err := db.Get([]byte("hello.005"))
	if err != nil {
		t.Fatal(err)
	}
	if err := closer.Close(); err != nil {
		t.Fatal(err)
	}
	if string(value) != "hello.005" {
		t.Fatal("set failed")
	}

	iter := db.NewIter(&pebble.IterOptions{
		LowerBound: []byte("hello.000"),
		UpperBound: []byte("hello.999"),
	})
	for iter.First(); iter.Valid(); iter.Next() {
		if err := db.Delete(iter.Key(), nil); err != nil {
			t.Fatal(err)
		}
	}
	if err := iter.Close(); err != nil {
		t.Fatal(err)
	}

	_, _, err = db.Get([]byte("hello.005"))
	if err != pebble.ErrNotFound {
		t.Fatal(err)
	}
}
