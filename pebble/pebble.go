package db

import (
	"bytes"
	"encoding/gob"
	"encoding/json"
	"log"

	"ant-service/db/rawalloc"

	"github.com/cockroachdb/pebble"
	"github.com/cockroachdb/pebble/bloom"
)

type Pebble struct {
	*pebble.DB
}

func NewPebble(dir string) (*Pebble, error) {
	cache := pebble.NewCache(128 << 20) // 128 MB
	defer cache.Unref()
	opts := &pebble.Options{
		Cache: cache,
		//Comparer:                    mvccComparer,
		//DisableWAL:                  disableWAL,
		FormatMajorVersion:          pebble.FormatNewest,
		L0CompactionThreshold:       2,
		L0StopWritesThreshold:       1000,
		LBaseMaxBytes:               64 << 20, // 64 MB
		Levels:                      make([]pebble.LevelOptions, 7),
		MaxConcurrentCompactions:    func() int { return 4 },
		MaxOpenFiles:                16384,
		MemTableSize:                64 << 20,
		MemTableStopWritesThreshold: 4,

		//Merger: &pebble.Merger{
		//	Name: "cockroach_merge_operator",
		//},
	}

	for i := 0; i < len(opts.Levels); i++ {
		l := &opts.Levels[i]
		l.BlockSize = 32 << 10       // 32 KB
		l.IndexBlockSize = 256 << 10 // 256 KB
		l.FilterPolicy = bloom.FilterPolicy(10)
		l.FilterType = pebble.TableFilter
		if i > 0 {
			l.TargetFileSize = opts.Levels[i-1].TargetFileSize * 2
		}
		l.EnsureDefaults()
	}
	opts.Levels[6].FilterPolicy = nil
	opts.FlushSplitBytes = opts.Levels[0].TargetFileSize

	opts.EnsureDefaults()
	//
	//if verbose {
	//	opts.EventListener = pebble.MakeLoggingEventListener(nil)
	//	opts.EventListener.TableDeleted = nil
	//	opts.EventListener.TableIngested = nil
	//	opts.EventListener.WALCreated = nil
	//	opts.EventListener.WALDeleted = nil
	//}

	db, err := pebble.Open(dir, opts)
	if err != nil {
		return nil, err
	}

	//return pebbleDB{
	//	d:       p,
	//	ballast: make([]byte, 1<<30),
	//}

	return &Pebble{
		db,
	}, nil
}

func (p *Pebble) SaveJson(key string, data interface{}) error {
	bs, err := json.Marshal(data)
	if err != nil {
		return err
	}
	return p.Set(String2Bytes(key), bs, nil)
}

func (p *Pebble) LoadJson(key string, value interface{}) error {
	bs, err := p.Get(String2Bytes(key))
	if err != nil {
		return err
	}
	return json.Unmarshal(bs, value)
}

func (p *Pebble) LoadJsonOrNil(key string, value interface{}) error {
	bs, err := p.GetOrNil(String2Bytes(key))
	if err != nil {
		return err
	}
	if bs == nil {
		return nil
	}
	return json.Unmarshal(bs, value)
}

func (p *Pebble) Exist(key string) (bool, error) {
	_, closer, err := p.DB.Get(String2Bytes(key))
	if err != nil {
		return false, err
	}
	if err := closer.Close(); err != nil {
		log.Printf("pebble exist close: %s", err)
		return false, err
	}
	return true, nil
}

func (p *Pebble) Get(key []byte) ([]byte, error) {
	bs, closer, err := p.DB.Get(key)
	if err != nil {
		return nil, err
	}
	defer func() {
		if err := closer.Close(); err != nil {
			log.Printf("pebble iter close: %s", err)
		}
	}()

	// 在调用 closer.Close() 之前使用 bs 才是安全的
	res := make([]byte, len(bs))
	copy(res, bs)
	return res, nil
}

func (p *Pebble) GetOrNil(key []byte) ([]byte, error) {
	if res, err := p.Get(key); NotFound(err) {
		return nil, nil
	} else {
		return res, err
	}
}

func (p Pebble) Scan(iter *pebble.Iterator, key []byte, count int64, reverse bool) (m [][][]byte) {
	var data rawalloc.A
	m = make([][][]byte, 0, count)
	if reverse {
		for i, valid := 0, iter.SeekLT(key); valid; valid = iter.Prev() {
			li := make([][]byte, 2)
			li[0], _ = data.Copy(iter.Key())
			li[1], _ = data.Copy(iter.Value())
			m = append(m, li)
			i++
			if i >= int(count) {
				break
			}
		}
	} else {
		for i, valid := 0, iter.SeekGE(key); valid; valid = iter.Next() {
			li := make([][]byte, 2)
			li[0], _ = data.Copy(iter.Key())
			li[1], _ = data.Copy(iter.Value())
			m = append(m, li)
			i++
			if i >= int(count) {
				break
			}
		}
	}
	return m
}

func (p *Pebble) NewBatch() *Batch {
	return &Batch{p.DB.NewBatch()}
}

func (p *Pebble) LastKey(prefix []byte, upper []byte) []byte {
	iter := p.NewIter(&pebble.IterOptions{
		LowerBound: prefix,
		UpperBound: upper,
	})
	iter.Last()
	if iter.Value() != nil {
		return iter.Key()
	}
	return []byte{}
}

func (p *Pebble) Iter(lowerBound, upperBound []byte) *pebble.Iterator {
	return p.DB.NewIter(&pebble.IterOptions{
		LowerBound: lowerBound,
		UpperBound: upperBound,
	})
}

type Batch struct {
	*pebble.Batch
}

func (b *Batch) SaveJson(key string, data interface{}) error {
	bs, err := json.Marshal(data)
	if err != nil {
		return err
	}
	return b.Set(String2Bytes(key), bs, nil)
}

func (b *Batch) LoadJson(key string, value interface{}) error {
	bs, closer, err := b.Get(String2Bytes(key))
	if err != nil {
		return err
	}
	defer func() {
		if err := closer.Close(); err != nil {
			log.Printf("pebble iter close: %s", err)
		}
	}()
	if err != nil {
		return err
	}
	return json.Unmarshal(bs, value)
}

func (b *Batch) SaveGob(key string, data interface{}) error {
	buf := &bytes.Buffer{}
	err := gob.NewEncoder(buf).Encode(data)
	if err != nil {
		return err
	}
	return b.Set(String2Bytes(key), buf.Bytes(), nil)
}

func (b *Batch) LoadGob(key string, value interface{}) error {
	bs, closer, err := b.Get(String2Bytes(key))
	if err != nil {
		return err
	}
	defer func() {
		if err := closer.Close(); err != nil {
			log.Printf("pebble iter close: %s", err)
		}
	}()
	buf := bytes.NewReader(bs)
	return gob.NewDecoder(buf).Decode(value)
}

func NotFound(err error) bool {
	return err == pebble.ErrNotFound
}
