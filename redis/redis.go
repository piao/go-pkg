package redis

import (
	"fmt"
	"reflect"
	"time"

	json "gitee.com/piao/go-pkg/json"
	redis "github.com/go-redis/redis"
)

const Nil = redis.Nil

type Redis struct {
	opt *redis.Options
	cli *redis.Client
}

// NewClient returns a client to the Redis Server specified by URL.
// parses an URL into Options that can be used to connect to Redis.
// e.g: "redis://<password>@<address>/<database>"
// e.g: "redis://:qwerty@localhost:6379/1"
// address is localhost:6379
// db is 1
// password is qwerty
func NewClient(url string) (*Redis, error) {
	opt, err := redis.ParseURL(url)
	if err != nil {
		err = fmt.Errorf("parse url %s err %w", url, err)
		return nil, err
	}

	// Create client as usually.
	rdb := redis.NewClient(opt)

	r := &Redis{
		opt: opt,
		cli: rdb,
	}
	if err := r.ping(); err != nil {
		return nil, err
	}
	return r, nil
}

func (r *Redis) ping() error {
	pong, err := r.cli.Ping().Result()
	if err != nil {
		err = fmt.Errorf("opt %+v ping err %w", r.opt, err)
		return err
	}
	if pong != "PONG" {
		err = fmt.Errorf("opt %+v ping failed", r.opt)
		return err
	}
	return nil
}

func (r *Redis) setCli() error {
	if r.cli != nil {
		if r.ping() == nil {
			return nil
		}
		r.cli.Close()
	}

	r.cli = redis.NewClient(r.opt)
	if err := r.ping(); err != nil {
		return err
	}
	return nil
}

func (r *Redis) Cli() *redis.Client {
	if err := r.setCli(); err != nil {
		return nil
	}
	return r.cli
}

func (r *Redis) Exists(key string) (val int64, err error) {
	if err = r.setCli(); err != nil {
		return
	}
	val, err = r.cli.Exists(key).Result()
	return
}

func (r *Redis) Get(key string, value interface{}) (err error) {
	if err = r.setCli(); err != nil {
		return
	}
	switch reflect.TypeOf(value).Kind() {
	case reflect.Struct,
		reflect.Ptr,
		reflect.Map,
		reflect.Slice:
		var val string
		if val, err = r.cli.Get(key).Result(); err != nil {
			return
		}
		err = json.Unmarshal([]byte(val), value)
		return
	}
	err = r.cli.Get(key).Scan(value)
	return
}

func (r *Redis) Set(key string, value interface{}, expiration time.Duration) (err error) {
	if err = r.setCli(); err != nil {
		return
	}
	switch reflect.TypeOf(value).Kind() {
	case reflect.Struct,
		reflect.Ptr,
		reflect.Map,
		reflect.Slice:
		var val []byte
		if val, err = json.Marshal(value); err != nil {
			return
		}
		err = r.cli.Set(key, string(val), expiration).Err()
		return
	}
	err = r.cli.Set(key, value, expiration).Err()
	return
}

func (r *Redis) Del(key string) (err error) {
	if err = r.setCli(); err != nil {
		return
	}
	err = r.cli.Del(key).Err()
	return
}

// key 不存在时设置(应用场景分布式锁)
func (r *Redis) SetNX(key string, value interface{}, expiration time.Duration) (err error) {
	if err = r.setCli(); err != nil {
		return
	}
	err = r.cli.SetNX(key, value, expiration).Err()
	return
}

// key 存在时设置
func (r *Redis) SetXX(key string, value interface{}, expiration time.Duration) (err error) {
	if err = r.setCli(); err != nil {
		return
	}
	err = r.cli.SetXX(key, value, expiration).Err()
	return
}

func (r *Redis) HGet(key, field string, value interface{}) (err error) {
	if err = r.setCli(); err != nil {
		return
	}
	switch reflect.TypeOf(value).Kind() {
	case reflect.Struct,
		reflect.Ptr,
		reflect.Map,
		reflect.Slice:
		var val string
		if val, err = r.cli.HGet(key, field).Result(); err != nil {
			return
		}
		err = json.Unmarshal([]byte(val), value)
		return
	}
	err = r.cli.HGet(key, field).Scan(value)
	return
}

func (r *Redis) HGetAll(key string) (val map[string]string, err error) {
	if err = r.setCli(); err != nil {
		return
	}
	val, err = r.cli.HGetAll(key).Result()
	return
}

func (r *Redis) HVals(key string) (val []string, err error) {
	if err = r.setCli(); err != nil {
		return
	}
	val, err = r.cli.HVals(key).Result()
	return
}

func (r *Redis) HKeys(key string) (val []string, err error) {
	if err = r.setCli(); err != nil {
		return
	}
	val, err = r.cli.HKeys(key).Result()
	return
}

func (r *Redis) HSet(key, field string, value interface{}) (err error) {
	if err = r.setCli(); err != nil {
		return
	}
	switch reflect.TypeOf(value).Kind() {
	case reflect.Struct,
		reflect.Ptr,
		reflect.Map,
		reflect.Slice:
		var val []byte
		if val, err = json.Marshal(value); err != nil {
			return
		}
		err = r.cli.HSet(key, field, string(val)).Err()
		return
	}
	err = r.cli.HSet(key, field, value).Err()
	return
}

func (r *Redis) HDel(key, field string) (err error) {
	if err = r.setCli(); err != nil {
		return
	}
	err = r.cli.HDel(key, field).Err()
	return
}

// field 不存在时设置
func (r *Redis) HSetNX(key, field string, value interface{}) (err error) {
	if err = r.setCli(); err != nil {
		return
	}
	err = r.cli.HSetNX(key, field, value).Err()
	return
}

func (r *Redis) LPush(key string, value interface{}) (val int64, err error) {
	if err = r.setCli(); err != nil {
		return
	}
	return r.cli.LPush(key, value).Result()
}

func (r *Redis) PTTL(key string) (val time.Duration, err error) {
	if err = r.setCli(); err != nil {
		return
	}
	return r.cli.PTTL(key).Result()
}

func (r *Redis) Expire(key string, exp time.Duration) (val bool, err error) {
	if err = r.setCli(); err != nil {
		return
	}
	return r.cli.Expire(key, exp).Result()
}

func (r *Redis) ZRem(key string, member interface{}) (val int64, err error) {
	if err = r.setCli(); err != nil {
		return
	}
	return r.cli.ZRem(key, member).Result()
}

func (r *Redis) ZAdd(key string, score float64, member interface{}) (val int64, err error) {
	if err = r.setCli(); err != nil {
		return
	}
	return r.cli.ZAdd(key, redis.Z{Score: score, Member: member}).Result()
}

func (r *Redis) ZIncrBy(key string, score float64, member string) (val float64, err error) {
	if err = r.setCli(); err != nil {
		return
	}
	return r.cli.ZIncrBy(key, score, member).Result()
}

func (r *Redis) ZRangeByScore(key, min, max string) (val []string, err error) {
	if err = r.setCli(); err != nil {
		return
	}
	return r.cli.ZRangeByScore(key, redis.ZRangeBy{Min: min, Max: max}).Result()
}

func (r *Redis) Subscribe(channels ...string) (ch <-chan *redis.Message, err error) {
	if err = r.setCli(); err != nil {
		return
	}
	pubsub := r.cli.Subscribe(channels...)
	if _, err = pubsub.Receive(); err != nil {
		return
	}
	ch = pubsub.Channel()
	return
}

func (r *Redis) Publish(channel string, message interface{}) (err error) {
	if err = r.setCli(); err != nil {
		return
	}
	err = r.cli.Publish(channel, message).Err()
	return
}
