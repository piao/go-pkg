package redis

import (
	"fmt"
	"time"

	"github.com/go-redis/redis"
)

// See http://redis.io/topics/cluster-tutorial for instructions
type ClusterRedis struct {
	opt *redis.ClusterOptions
	cli *redis.ClusterClient
}

// NewClusterClient returns a Redis Cluster client as described in http://redis.io/topics/cluster-spec.
// e.g: []string{":7000", ":7001", ":7002"}
func NewClusterClient(addrs []string) (*ClusterRedis, error) {
	opt := &redis.ClusterOptions{
		Addrs: addrs,
	}

	rdb := redis.NewClusterClient(opt)

	r := &ClusterRedis{
		opt: opt,
		cli: rdb,
	}
	if err := r.ping(); err != nil {
		return nil, err
	}
	return r, nil
}

func (r *ClusterRedis) ping() error {
	pong, err := r.cli.Ping().Result()
	if err != nil {
		err = fmt.Errorf("opt %+v ping err %w", r.opt, err)
		return err
	}
	if pong != "PONG" {
		err = fmt.Errorf("opt %+v ping failed", r.opt)
		return err
	}
	return nil
}

func (r *ClusterRedis) setCli() error {
	if r.cli != nil {
		if r.ping() == nil {
			return nil
		}
		r.cli.Close()
	}

	r.cli = redis.NewClusterClient(r.opt)
	if err := r.ping(); err != nil {
		return err
	}
	return nil
}

func (r *ClusterRedis) Cli() *redis.ClusterClient {
	if err := r.setCli(); err != nil {
		return nil
	}
	return r.cli
}

func (r *ClusterRedis) Get(key string, value interface{}) (err error) {
	if err = r.setCli(); err != nil {
		return
	}
	err = r.cli.Get(key).Scan(value)
	return
}

func (r *ClusterRedis) Set(key string, value interface{}, expiration time.Duration) (err error) {
	if err = r.setCli(); err != nil {
		return
	}
	err = r.cli.Set(key, value, expiration).Err()
	return
}

// key 不存在时设置(应用场景分布式锁)
func (r *ClusterRedis) SetNX(key string, value interface{}, expiration time.Duration) (err error) {
	if err = r.setCli(); err != nil {
		return
	}
	err = r.cli.SetNX(key, value, expiration).Err()
	return
}

// key 存在时设置
func (r *ClusterRedis) SetXX(key string, value interface{}, expiration time.Duration) (err error) {
	if err = r.setCli(); err != nil {
		return
	}
	err = r.cli.SetXX(key, value, expiration).Err()
	return
}

func (r *ClusterRedis) HGet(key, field string, value interface{}) (err error) {
	if err = r.setCli(); err != nil {
		return
	}
	err = r.cli.HGet(key, field).Scan(value)
	return
}

func (r *ClusterRedis) HSet(key, field string, value interface{}) (err error) {
	if err = r.setCli(); err != nil {
		return
	}
	err = r.cli.HSet(key, field, value).Err()
	return
}

// field 不存在时设置
func (r *ClusterRedis) HSetNX(key, field string, value interface{}) (err error) {
	if err = r.setCli(); err != nil {
		return
	}
	err = r.cli.HSetNX(key, field, value).Err()
	return
}
