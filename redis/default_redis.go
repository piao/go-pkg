package redis

import (
	"github.com/go-redis/redis"
)

// NewClient returns a client to the Redis Server specified by Options.
func NewDefaultClient() (*Redis, error) {
	opt := &redis.Options{
		Addr:     "localhost:6379", // use default Addr
		Password: "",               // no password set
		DB:       0,                // use default DB
	}

	// Create client as usually.
	rdb := redis.NewClient(opt)

	r := &Redis{
		opt: opt,
		cli: rdb,
	}
	if err := r.ping(); err != nil {
		return nil, err
	}
	return r, nil
}
