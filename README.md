## go-pkg

    "go common package"

## Installation
```sh

$ go get gitee.com/piao/go-pkg

```

## Documentation

- gopool 协程池
- json json包 go build -tags=jsoniter
- mysql mysql 连接
- redis redis 连接
- signal signal 监听


## TODO

* test
* fix bug

## 参考

- [ws-examples](https://github.com/piaohua/ws-examples)
- [pkg](https://github.com/hb-go/pkg)
- [dashboard](https://github.com/hb-go/conn)
