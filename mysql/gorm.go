package mysql

import (
	"fmt"

	"github.com/jinzhu/gorm"
	_ "github.com/jinzhu/gorm/dialects/mysql"
)

type MysqlDB struct {
	url string
	db  *gorm.DB
}

// url e.g: user:password@(localhost)/dbname?charset=utf8&parseTime=True&loc=Local
func NewMysqlDB(url string) (*MysqlDB, error) {
	db, err := gorm.Open("mysql", url)
	if err != nil {
		err = fmt.Errorf("open url %s err %w", url, err)
		return nil, err
	}

	s := &MysqlDB{
		url: url,
		db:  db,
	}
	s.DefaultSet()

	return s, nil
}

func (s *MysqlDB) ping() error {
	if err := s.db.DB().Ping(); err != nil {
		err = fmt.Errorf("url %s ping err %w", s.url, err)
		return err
	}
	return nil
}

func (s *MysqlDB) setDB() error {
	if s.db != nil {
		if s.ping() == nil {
			return nil
		}
		s.db.Close()
	}

	db, err := gorm.Open("mysql", s.url)
	if err != nil {
		err = fmt.Errorf("open s.url %s err %w", s.url, err)
		return err
	}
	s.db = db
	s.DefaultSet()
	if err := s.ping(); err != nil {
		return err
	}
	return nil
}

func (s *MysqlDB) C() (*gorm.DB, error) {
	if err := s.setDB(); err != nil {
		return nil, err
	}
	return s.db, nil
}

func (s *MysqlDB) CreateTable(models ...interface{}) {
	s.db.Set("gorm:table_options", "ENGINE=InnoDB").CreateTable(models...)
}

func (s *MysqlDB) AutoMigrate(models ...interface{}) {
	s.db.Set("gorm:table_options", "ENGINE=InnoDB").AutoMigrate(models...)
}

func (s *MysqlDB) Create(value interface{}) error {
	if err := s.setDB(); err != nil {
		return err
	}
	return s.db.Create(value).Error
}

func (s *MysqlDB) Save(value interface{}) error {
	if err := s.setDB(); err != nil {
		return err
	}
	return s.db.Save(value).Error
}

func (s *MysqlDB) Update(value, query interface{}, fields []interface{}, args ...interface{}) error {
	if err := s.setDB(); err != nil {
		return err
	}
	return s.db.Model(value).Where(query, args...).Update(fields...).Error
}

func (s *MysqlDB) Updates(value, values, query interface{}, args ...interface{}) error {
	if err := s.setDB(); err != nil {
		return err
	}
	return s.db.Model(value).Where(query, args...).Updates(values).Error
}

func (s *MysqlDB) First(value interface{}, query interface{}, args ...interface{}) error {
	if err := s.setDB(); err != nil {
		return err
	}
	return s.db.Where(query, args...).First(value).Error
}

func (s *MysqlDB) FindAll(value interface{}) error {
	if err := s.setDB(); err != nil {
		return err
	}
	return s.db.Find(value).Error
}

func (s *MysqlDB) Find(value interface{}, query interface{}, args ...interface{}) error {
	if err := s.setDB(); err != nil {
		return err
	}
	return s.db.Where(query, args...).Find(value).Error
}
