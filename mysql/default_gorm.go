package mysql

import (
	"fmt"
	"reflect"
	"time"

	json "gitee.com/piao/go-pkg/json"
	gorm "github.com/jinzhu/gorm"
	log "github.com/sirupsen/logrus"
)

func (s *MysqlDB) DefaultSet() {
	// SetMaxIdleCons 设置连接池中的最大闲置连接数。
	s.db.DB().SetMaxIdleConns(10)

	// SetMaxOpenCons 设置数据库的最大连接数量。
	s.db.DB().SetMaxOpenConns(100)

	// SetConnMaxLifetiment 设置连接的最大可复用时间。
	s.db.DB().SetConnMaxLifetime(time.Hour)

	// SetLogger
	s.db.LogMode(true)
	logger := log.StandardLogger()
	logger.SetFormatter(&log.JSONFormatter{})
	s.db.SetLogger(gorm.Logger{logger})

	s.db.SingularTable(true)
}

/*
type Data struct {
	Args Args `gorm:"type:text"`
}

type Args []int64

func (s Args) Scan(value interface{}) error {
	return scan(&s, value)
}

func (s Args) Value() (driver.Value, error) {
	return value(s)
}
*/
// Scan for scanner helper
func Scan(data interface{}, value interface{}) error {
	if value == nil {
		return nil
	}

	switch value.(type) {
	case []byte:
		return json.Unmarshal(value.([]byte), data)
	case string:
		return json.Unmarshal([]byte(value.(string)), data)
	default:
		return fmt.Errorf("val type is valid, is %+v", value)
	}
}

// for valuer helper
func Value(data interface{}) (interface{}, error) {
	vi := reflect.ValueOf(data)
	// 判断是否为 0 值
	if vi.IsZero() {
		return nil, nil
	}
	return json.Marshal(data)
}
